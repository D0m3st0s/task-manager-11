FROM java:openjdk18
COPY ./target/task-manager-1.0-SNAPSHOT.jar /opt/task-manager-1.0-SNAPSHOT.jar
WORKDIR /opt

ENTRYPOINT ["java", "-jar", "task-manager-1.0-SNAPSHOT.jar"]