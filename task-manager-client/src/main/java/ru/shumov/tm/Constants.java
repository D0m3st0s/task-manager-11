package ru.shumov.tm;

public class Constants {

    public final static String WELCOME = "       Добро Пожаловать в Task Manager";
    public final static String PROJECT_ID = "...ID Проекта Можно Найти в Списке Проектов...";

    public final static String YES_NO = "yes/no";
    public final static String YES = "yes";
    public final static String DONE = "Done";

    public final static String ENTER_PROJECT_NAME = "Введите имя проекта:";
    public final static String ENTER_PROJECT_ID_FOR_TASKS = "Чтобы добавить задание к проекту введите id проекта:";
    public final static String ENTER_PROJECT_ID_FOR_REMOVING = "Введите ID Проекта который вы хотите удалить:";
    public final static String ENTER_TASK_ID_FOR_SHOWING_TASKS = "Введите id задачи которого нужно вывести:";
    public final static String ENTER_PROJECT_ID = "Введите id проекта который хотите вывести:";
    public final static String ENTER_DEADLINE_OF_PROJECT = "Введите дату окончания работы над проектом:";
    public final static String ENTER_START_DATE_OF_PROJECT = "Введите дату начала работы над проектом:";
    public final static String ENTER_DESCRIPTION_OF_PROJECT = "Введите описание проекта:";
    public final static String ENTER_ID_OF_PROJECT_FOR_SHOWING = "Введите id проекта который хотите изменить.";
    public final static String ENTER_TASK_NAME = "Введите назввание задачи:";
    public final static String ENTER_TASK_ID_FOR_REMOVING = "Введите id задачи которую вы хотите удалить:";
    public final static String ENTER_DEADLINE_OF_TASK = "Введите дату окончания работы над заданием:";
    public final static String ENTER_START_DATE_OF_TASK = "Введите дату начала работы над заданием:";
    public final static String ENTER_DESCRIPTION_OF_TASK = "Введите описание задания:";
    public final static String ENTER_ID_OF_TASK_FOR_SHOWING = "Введите id задания который хотите изменить.";
    public final static String ENTER_NAME_OF_USER = "Введите имя пользователя:";
    public final static String ENTER_PASSWORD = "Введите пароль:";
    public final static String ENTER_NEW_PASSWORD = "Введите новый пароль:";
    public final static String ENTER_OLD_PASSWORD = "Введите старый пароль:";
    public final static String ENTER_LOGIN = "Введите логин:";

    public final static String PROJECT_DOES_NOT_EXIST = "Такого проекта нет.";
    public final static String PROJECTS_DO_NOT_EXIST = "Проектов нет";
    public final static String COMMAND_DOES_NOT_EXIST = "Такой команды нет.";
    public final static String TASKS_DO_NOT_EXIST = "Задач нет.";
    public final static String TASK_DOES_NOT_EXIST = "Такой задачи нет.";

    public final static String ALL_PROJECTS_WILL_BE_CLEARED = "Все проекты будут удалены, Вы уверны?";
    public final static String ALL_TASKS_WILL_BE_CLEARED = "Все задачи будут удалены, Вы уверны?";

    public final static String EXCEPTION_PROJECT = "Некорректные данные проекта : ";
    public final static String EXCEPTION_TASK = "Некорректные данные задачи : ";

    public final static String NO_ROOTS = "У вас недостаточно прав.";
    public final static String NO_ROOTS_FOR_PROJECT = "У вас нет доступа к этому проекту.";
    public final static String INCORRECT_DATE_FORMAT = "Incorrect Date format";
    public final static String INVALID_PASSWORD = "Пароль неверный";
    public final static String PASSWORD_UPDATE_SUCCESSFUL = "Пароль успешно изменён.";
    public final static String INVALID_LOGIN = "Логин неверный";

    public final static String AUTHORIZATION_SUCCESSFUL = "Авторизация прошла успешно.";
    public final static String LOG_OUT_SUCCESSFUL = "Выход из аккаунта прошёл успешно.";
    public final static String REGISTRATION_SUCCESSFUL = "Регистрация прошла успешно.";
    public final static String USER_ALREADY_AUTHORIZED = "Пользователь уже авторизован.";
    public final static String USER_DID_NOT_AUTHORIZED = "Пользователь не авторизован.";
    public final static String USER_ALREADY_EXIST = "Такой пользователь уже есть.";

    public final static String BY_CREATING_DATE = "by creating date";
    public final static String BY_START_DATE = "by start date";
    public final static String BY_END_DATE = "by end date";
    public final static String BY_STATUS = "by status";
}
