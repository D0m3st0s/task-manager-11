package ru.shumov.tm.command;

import lombok.Getter;

@Getter
public class Exit extends AbstractCommand {
    private final String name = "exit";
    private final String description = "exit: Остановка программы.";

    @Override
    public void execute() {
        bootstrap.setWork(false);
    }

    public Exit() {
    }
}
