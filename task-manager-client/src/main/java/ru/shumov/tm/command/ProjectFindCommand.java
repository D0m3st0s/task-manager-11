package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.project.Project;
import ru.shumov.tm.endpoint.user.Role;

import java.util.Collection;

public class ProjectFindCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "find project";
    @Getter
    private final String description = "find project: Поиск проекта по названию или описанию.";

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString("Введите название или описание проекта");
            final var answer = bootstrap.getTerminalService().scanner();
            Collection<Project> values = bootstrap.getProjectEndPoint().getList(session);
            for (Project project : values) {
                if (project.getName().contains(answer) || project.getDescription().contains(answer)) {
                    bootstrap.getTerminalService()
                            .outPutString(bootstrap.getToStringService().projectToString(project));
                }
            }
        }
    }
}
