package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.Status;
import ru.shumov.tm.endpoint.project.Project;
import ru.shumov.tm.endpoint.user.Role;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class ProjectUpdateCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "project update";
    @Getter
    private final String description = "project update: Изменение параметров проекта.";

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        var project = new Project();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            bootstrap.getTerminalService().outPutString(Constants.ENTER_ID_OF_PROJECT_FOR_SHOWING);
            @NotNull final var projectId = bootstrap.getTerminalService().scanner();
            if (bootstrap.getProjectEndPoint().getOne(session,projectId) == null) {
                bootstrap.getTerminalService().outPutString(Constants.PROJECT_DOES_NOT_EXIST);
            } else {
                project = bootstrap.getProjectEndPoint().getOne(session, projectId);
            }
            if (!project.getUserId().equals(user.getId())) {
                bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS_FOR_PROJECT);
                return;
            }
            try {
                GregorianCalendar calendar = new GregorianCalendar();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_PROJECT_NAME);
                @NotNull final var name = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString(Constants.ENTER_START_DATE_OF_PROJECT);
                @NotNull final var startDate = bootstrap.getTerminalService().scanner();
                calendar.setTime(format.parse(startDate));
                XMLGregorianCalendar XMLStartDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
                project.setStartDate(XMLStartDate);
                bootstrap.getTerminalService().outPutString(Constants.ENTER_DEADLINE_OF_PROJECT);
                @NotNull final var endDate = bootstrap.getTerminalService().scanner();
                calendar.setTime(format.parse(endDate));
                XMLGregorianCalendar XMLEndDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
                project.setEndDate(XMLEndDate);
                bootstrap.getTerminalService().outPutString(Constants.ENTER_DESCRIPTION_OF_PROJECT);
                @NotNull final var description = bootstrap.getTerminalService().scanner();
                bootstrap.getTerminalService().outPutString("Введите статус проекта:");
                bootstrap.getTerminalService().outPutString("planned/in process/done");
                @NotNull final var status = bootstrap.getTerminalService().scanner();
                switch (status) {
                    case ("planned"):
                        project.setStatus(Status.PLANNED);
                        break;
                    case ("in process"):
                        project.setStatus(Status.IN_PROCESS);
                        break;
                    case ("done"):
                        project.setStatus(Status.DONE);
                        break;
                }

                project.setName(name);
                project.setDescription(description);;
                bootstrap.getProjectEndPoint().update(session, project);
                bootstrap.getTerminalService().outPutString(Constants.DONE);
            } catch (ParseException parseException) {
                bootstrap.getTerminalService().outPutString(Constants.INCORRECT_DATE_FORMAT);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public ProjectUpdateCommand() {
    }
}
