package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.user.Role;
public class TaskClearCommand extends AbstractCommand {
    private final Role role = Role.ADMINISTRATOR;
    @Getter
    private final String name = "task clear";
    @Getter
    private final String description = "task clear: Удаление всех задач.";

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ALL_TASKS_WILL_BE_CLEARED);
            bootstrap.getTerminalService().outPutString(Constants.YES_NO);
            @NotNull final var answer = bootstrap.getTerminalService().scanner();
            if (Constants.YES.equals(answer)) {
                bootstrap.getTaskEndPoint().clear(session);
            }
            bootstrap.getTerminalService().outPutString(Constants.DONE);
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public TaskClearCommand() {
    }
}
