package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.endpoint.user.Role;
public class TaskGetOneCommand extends AbstractCommand {
    private final Role role = Role.USER;
    @Getter
    private final String name = "get task";
    @Getter
    private final String description = "get task: вывод конкретной задачи.";

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            return;
        }
        if (user.getRole().equals(role)) {
            bootstrap.getTerminalService().outPutString(Constants.ENTER_TASK_ID_FOR_SHOWING_TASKS);
            @NotNull final var taskId = bootstrap.getTerminalService().scanner();
            @NotNull final var task = bootstrap.getTaskEndPoint().getOne(session, taskId);
            if (task.getUserId().equals(user.getId())) {
                bootstrap.getTerminalService()
                        .outPutString(bootstrap.getToStringService().taskToString(task));
            } else {
                bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
            }
        } else {
            bootstrap.getTerminalService().outPutString(Constants.NO_ROOTS);
        }
    }

    public TaskGetOneCommand() {
    }
}
