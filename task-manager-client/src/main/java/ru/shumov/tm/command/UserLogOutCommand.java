package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;

public class UserLogOutCommand extends AbstractCommand {
    @Getter
    private final String name = "log out";
    @Getter
    private final String description = "log out: Выход из учётной записи";

    @SneakyThrows
    public void execute() {
        @Nullable final var user = bootstrap.getUser();
        @Nullable final var session = bootstrap.getSession();
        if (user == null) {
            bootstrap.getTerminalService().outPutString(Constants.USER_DID_NOT_AUTHORIZED);
            return;
        }
        bootstrap.setUser(null);
        bootstrap.getSessionEndPoint().closeSession(session);
        bootstrap.setSession(null);
        bootstrap.getTerminalService().outPutString(Constants.LOG_OUT_SUCCESSFUL);
    }

    public UserLogOutCommand() {
    }
}
