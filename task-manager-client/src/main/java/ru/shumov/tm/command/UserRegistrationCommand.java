package ru.shumov.tm.command;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.dto.UserDTO;

import java.security.NoSuchAlgorithmException;

@Getter
public class UserRegistrationCommand extends AbstractCommand {
    private final String name = "registration";
    private final String description = "registration: Регистрация нового пользователя.";

    @SneakyThrows
    public void execute() throws NoSuchAlgorithmException {
        @Nullable var user = bootstrap.getUser();
        if (user != null) {
            return;
        }
        bootstrap.getTerminalService().outPutString(Constants.ENTER_LOGIN);
        @NotNull final var login = bootstrap.getTerminalService().scanner();
        if (bootstrap.getUserEndPoint().getOne(login) != null) {
            bootstrap.getTerminalService().outPutString(Constants.USER_ALREADY_EXIST);
            return;
        }
        bootstrap.getTerminalService().outPutString(Constants.ENTER_PASSWORD);
        @NotNull final var password = bootstrap.getTerminalService().scanner();
        if (password == null || password.isEmpty()) {
            bootstrap.getTerminalService().outPutString(Constants.INVALID_PASSWORD);
            return;
        }
        bootstrap.getUserEndPoint().create(login, password);
        bootstrap.getTerminalService().outPutString(Constants.REGISTRATION_SUCCESSFUL);
    }

    public UserRegistrationCommand() {
    }
}
