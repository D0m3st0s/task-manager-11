
package ru.shumov.tm.endpoint.user;

import javax.xml.namespace.QName;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;
import ru.shumov.tm.endpoint.Session;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.shumov.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final QName _Create_QNAME = new QName("http://endpoint.tm.shumov.ru/", "create");
    private static final QName _CreateResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "createResponse");
    private static final QName _GetOne_QNAME = new QName("http://endpoint.tm.shumov.ru/", "getOne");
    private static final QName _GetOneResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "getOneResponse");
    private static final QName _PasswordUpdate_QNAME = new QName("http://endpoint.tm.shumov.ru/", "passwordUpdate");
    private static final QName _PasswordUpdateResponse_QNAME = new QName("http://endpoint.tm.shumov.ru/", "passwordUpdateResponse");
    private static final QName _User_QNAME = new QName("http://endpoint.tm.shumov.ru/", "user");
    private static final QName _Exception_QNAME = new QName("http://endpoint.tm.shumov.ru/", "Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.shumov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Create }
     * 
     * @return
     *     the new instance of {@link Create }
     */
    public Create createCreate() {
        return new Create();
    }

    /**
     * Create an instance of {@link CreateResponse }
     * 
     * @return
     *     the new instance of {@link CreateResponse }
     */
    public CreateResponse createCreateResponse() {
        return new CreateResponse();
    }

    /**
     * Create an instance of {@link GetOne }
     * 
     * @return
     *     the new instance of {@link GetOne }
     */
    public GetOne createGetOne() {
        return new GetOne();
    }

    /**
     * Create an instance of {@link GetOneResponse }
     * 
     * @return
     *     the new instance of {@link GetOneResponse }
     */
    public GetOneResponse createGetOneResponse() {
        return new GetOneResponse();
    }

    /**
     * Create an instance of {@link PasswordUpdate }
     * 
     * @return
     *     the new instance of {@link PasswordUpdate }
     */
    public PasswordUpdate createPasswordUpdate() {
        return new PasswordUpdate();
    }

    /**
     * Create an instance of {@link PasswordUpdateResponse }
     * 
     * @return
     *     the new instance of {@link PasswordUpdateResponse }
     */
    public PasswordUpdateResponse createPasswordUpdateResponse() {
        return new PasswordUpdateResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     * @return
     *     the new instance of {@link User }
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     * @return
     *     the new instance of {@link Exception }
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link Session }
     * 
     * @return
     *     the new instance of {@link Session }
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Create }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Create }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "create")
    public JAXBElement<Create> createCreate(Create value) {
        return new JAXBElement<>(_Create_QNAME, Create.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "createResponse")
    public JAXBElement<CreateResponse> createCreateResponse(CreateResponse value) {
        return new JAXBElement<>(_CreateResponse_QNAME, CreateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOne }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetOne }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "getOne")
    public JAXBElement<GetOne> createGetOne(GetOne value) {
        return new JAXBElement<>(_GetOne_QNAME, GetOne.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOneResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetOneResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "getOneResponse")
    public JAXBElement<GetOneResponse> createGetOneResponse(GetOneResponse value) {
        return new JAXBElement<>(_GetOneResponse_QNAME, GetOneResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PasswordUpdate }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PasswordUpdate }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "passwordUpdate")
    public JAXBElement<PasswordUpdate> createPasswordUpdate(PasswordUpdate value) {
        return new JAXBElement<>(_PasswordUpdate_QNAME, PasswordUpdate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PasswordUpdateResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PasswordUpdateResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "passwordUpdateResponse")
    public JAXBElement<PasswordUpdateResponse> createPasswordUpdateResponse(PasswordUpdateResponse value) {
        return new JAXBElement<>(_PasswordUpdateResponse_QNAME, PasswordUpdateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link User }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "user")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<>(_User_QNAME, User.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<>(_Exception_QNAME, Exception.class, null, value);
    }

}
