package ru.shumov.tm.bootstrap;

import jakarta.xml.ws.Endpoint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBContextProperties;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import ru.shumov.tm.dto.UserDTO;
import ru.shumov.tm.endpoint.*;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.repository.*;
import ru.shumov.tm.service.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
public class Bootstrap {

    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();
    private final TaskRepository taskRepository = new TaskRepositoryImpl();
    private final UserRepository userRepository = new UserRepositoryImpl();
    private final Md5Service md5Service = new Md5ServiceImpl();
    private final UserService userService = new UserServiceImpl(userRepository, md5Service);
    private final TaskService taskService = new TaskServiceImpl(taskRepository);
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository, taskService);
    private final SessionService sessionService = new SessionServiceImpl(this);

    public void main() {
        usersLoad();
        publishEndpoints();
    }
    public void usersLoad() {
        try {
            Map<String, Object> properties = new HashMap<>();
            properties.put(JAXBContextProperties.MEDIA_TYPE, "application/json");

            var jaxbContextProjectDTO = JAXBContextFactory.createContext(new Class[]{UserDTO.class}, properties);
            var unMarshallerProjectDTO = jaxbContextProjectDTO.createUnmarshaller();
            unMarshallerProjectDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            var userDTO = (UserDTO) unMarshallerProjectDTO.unmarshal(new File("JAXBJsonUsers.json"));
            var users = userDTO.getUsers();
            for (User obj : users) {
                System.out.println(obj);
                getUserService().update(obj);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void publishEndpoints(){
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl",
                new ProjectEndPoint(projectService, sessionService));
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl",
                new TaskEndPoint(taskService, sessionService));
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl",
                new UserEndPoint(sessionService, userService));
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl",
                new SessionEndPoint(sessionService));

        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl");
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl");
        System.out.println("http://localhost:8080/UserEndpoint?wsdl");
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl");
    }


}
