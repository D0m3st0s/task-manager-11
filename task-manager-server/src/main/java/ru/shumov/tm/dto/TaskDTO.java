package ru.shumov.tm.dto;


import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.entity.Task;

import jakarta.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
public class TaskDTO implements Serializable {
    private List<Task> tasks = new ArrayList<>();
}
