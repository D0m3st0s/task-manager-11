package ru.shumov.tm.endpoint;


import lombok.NoArgsConstructor;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.entity.Session;
import ru.shumov.tm.exceptions.EndpointException;
import ru.shumov.tm.service.ProjectService;
import ru.shumov.tm.service.SessionService;

import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
public class ProjectEndPoint {

    private ProjectService projectService;
    private SessionService sessionService;

    public ProjectEndPoint(ProjectService projectService, SessionService sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    public void create(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "project") final Project project) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        projectService.create(project);
    }

    public List<Project> getList(
            @WebParam(name = "session") final Session session) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return projectService.getList(session.getUserId());
    }

    public Project getOne(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String projectId) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return projectService.getOne(projectId);
    }

    public void update(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "project") final Project project) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        projectService.update(project);
    }

    public List<Project> getSortedList(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "method") final String method) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return projectService.getSortedList(session.getUserId(), method);
    }

    public void remove(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "projectId") final String projectId) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        projectService.remove(projectId);
    }

    public void clear(
            @WebParam(name = "session") final Session session) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        projectService.clear();
    }

    public List<Project> find(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "part") final String part) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return projectService.find(session.getUserId(), part);
    }
}
