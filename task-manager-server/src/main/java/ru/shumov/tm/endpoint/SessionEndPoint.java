package ru.shumov.tm.endpoint;

import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.NoArgsConstructor;
import ru.shumov.tm.entity.Session;
import ru.shumov.tm.exceptions.EndpointException;
import ru.shumov.tm.service.SessionService;

@NoArgsConstructor
@WebService
public class SessionEndPoint {
    private SessionService sessionService;

    public SessionEndPoint(SessionService sessionService) {
        this.sessionService = sessionService;
    }

    public Session openSession(
            @WebParam(name = "login") final String login) throws Exception {
        if(sessionService == null) throw new EndpointException();
        return sessionService.openSession(login);
    }
    public void closeSession(
            @WebParam(name = "session") final Session session) throws Exception {
        if(sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        sessionService.closeSession(session);
    }
}
