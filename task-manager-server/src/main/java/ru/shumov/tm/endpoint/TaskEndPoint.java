package ru.shumov.tm.endpoint;

import lombok.NoArgsConstructor;
import ru.shumov.tm.entity.Session;
import ru.shumov.tm.entity.Task;
import ru.shumov.tm.exceptions.EndpointException;
import ru.shumov.tm.service.SessionService;
import ru.shumov.tm.service.TaskService;

import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
public class TaskEndPoint {
    private TaskService taskService;
    private SessionService sessionService;

    public TaskEndPoint(TaskService taskService, SessionService sessionService) {
        this.taskService = taskService;
        this.sessionService = sessionService;
    }

    public void create(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "task") final Task task) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        taskService.create(task);
    }
    public List<Task> getList(
            @WebParam(name = "session") final Session session) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return taskService.getList(session.getUserId());
    }
    public List<Task> getSortedList(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "method") final String method) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return taskService.getSortedList(session.getUserId(), method);
    }
    public Task getOne(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String taskId) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return taskService.getOne(taskId);
    }
    public void update(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "task") final Task task) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        taskService.update(task);
    }
    public void remove(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "taskId") final String taskId) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        taskService.remove(taskId);
    }
    public void clear(
            @WebParam(name = "session") final Session session) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        taskService.clear();
    }
    public List<Task> find(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "part") final String part) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointException();
        sessionService.validate(session);
        return taskService.find(session.getUserId(), part);
    }
}
