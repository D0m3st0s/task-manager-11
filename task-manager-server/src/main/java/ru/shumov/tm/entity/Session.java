package ru.shumov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class Session implements Cloneable {

    public Session clone() {
        try {
            return (Session) super.clone();
        }
        catch (CloneNotSupportedException e) {
            return null;
        }
    }
    @NotNull
    private String id = UUID.randomUUID().toString();
    @Nullable
    private Long timestamp;
    @Nullable
    private String userId;
    @Nullable
    private String signature;
    @Nullable
    private String ownerId;
}
