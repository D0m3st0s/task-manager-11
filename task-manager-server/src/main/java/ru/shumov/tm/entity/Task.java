package ru.shumov.tm.entity;


import lombok.Getter;
import lombok.Setter;
import ru.shumov.tm.enums.Status;

import jakarta.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@XmlRootElement
public class Task implements Serializable {
    private String id;
    private String name;
    private String description;
    private Date creatingDate;
    private Date startDate;
    private Date endDate;
    private String projectId;
    private String userId;
    private Status status = Status.PLANNED;
}
