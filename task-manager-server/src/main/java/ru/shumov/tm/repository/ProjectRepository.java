package ru.shumov.tm.repository;

import ru.shumov.tm.entity.Project;

import java.util.Collection;
import java.util.List;

public interface ProjectRepository {

    Collection<Project> findAll();
    List<Project> findAll(String id);

    Project findOne(String id);

    void persist(String id, Project project);

    void merge(Project project);

    void remove(String id);

    void removeAll();
}
