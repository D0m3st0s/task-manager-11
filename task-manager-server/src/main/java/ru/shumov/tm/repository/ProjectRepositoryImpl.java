package ru.shumov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Project;

import java.util.*;

public class ProjectRepositoryImpl implements ProjectRepository {

    private Map<String, Project> projects = new HashMap<>();

    public Collection<Project> findAll() {
        return projects.values();
    }

    public List<Project> findAll(String id) {
        final Collection<Project> values = projects.values();
        final List<Project> projects = new ArrayList<>();
        for(Project project : values) {if(project.getUserId().equals(id)){projects.add(project);}}
        return projects;
    }

    public Project findOne(@NotNull String id) {
        return projects.get(id);
    }

    public void persist(@NotNull String id,@NotNull Project project) {
        if(!projects.containsKey(id)){
            projects.put(id, project);
        }
    }

    public void merge(@NotNull Project project) {
        projects.put(project.getId(), project);
    }

    public void remove(@NotNull String id) {
        projects.remove(id);
    }

    public void removeAll() {
        projects.clear();
    }
}
