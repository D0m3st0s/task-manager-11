package ru.shumov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Session;

import java.util.Collection;

public interface SessionRepository {

    Collection<Session> findAll();

    Session findOne(@NotNull String id);

    void persist(@NotNull Session session);

    void merge(@NotNull Session session);

    void remove(@NotNull String id);

    void removeAll();

    boolean checkKey(@NotNull final String id);
}
