package ru.shumov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Session;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class SessionRepositoryImpl implements SessionRepository {

    private Map<String, Session> sessions = new HashMap<>();
    public Collection<Session> findAll() {
        return sessions.values();
    }
    public Session findOne(@NotNull final String id) {
        return sessions.get(id);
    }
    public void persist(@NotNull final Session session) {
        if(!sessions.containsKey(session.getId())) {
            sessions.put(session.getId(), session);
        }
    }
    public void merge(@NotNull final Session session) {
        sessions.put(session.getId(), session);
    }
     public void remove(@NotNull final String id) {
        sessions.remove(id);
     }
     public void removeAll() {
        sessions.clear();
     }
     public boolean checkKey(@NotNull final String id) {
        return sessions.containsKey(id);
     }
}
