package ru.shumov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface TaskRepository {

    Collection<Task> findAll();

    List<Task> findAll(@NotNull String id);

    Task findOne(String id);

    void persist(Task task);

    void merge(Task task);

    void remove(String id);

    void removeAll();
}
