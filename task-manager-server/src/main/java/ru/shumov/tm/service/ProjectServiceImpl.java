package ru.shumov.tm.service;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shumov.tm.Constants;
import ru.shumov.tm.entity.Project;
import ru.shumov.tm.exceptions.IncorrectInputException;
import ru.shumov.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public class ProjectServiceImpl implements ProjectService {
    private ProjectRepository projectRepository;
    private TaskService taskService;


    public ProjectServiceImpl(ProjectRepository projectRepository,TaskService taskService) {
        this.projectRepository = projectRepository;
        this.taskService = taskService;
    }

    public void create(@NotNull Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project name");
            }
            projectRepository.persist(project.getId(), project);
        }
        catch (IncorrectInputException incorrectInput) {
            incorrectInput.printStackTrace();
        }
    }

    public void clear() {
        projectRepository.removeAll();
        taskService.clear();
    }

    public void remove(@Nullable String projectId) {
        try {
            if (projectId == null || projectId.isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project id");
            }
            projectRepository.remove(projectId);
        }
        catch (IncorrectInputException incorrectInput) {
            incorrectInput.printStackTrace();
        }
    }

    public Collection<Project> getList() {
        return projectRepository.findAll();
    }
    public List<Project> getList(String id) {
        return projectRepository.findAll(id);
    }

    public Project getOne(@NotNull String id) {
        return projectRepository.findOne(id);
    }

    public void update(@NotNull Project project) {
        try {
            if (project.getName() == null || project.getName().isEmpty()) {
                throw new IncorrectInputException(Constants.EXCEPTION_PROJECT + "project name");
            }
            projectRepository.merge(project);
        }
        catch (IncorrectInputException incorrectInput) {
            incorrectInput.printStackTrace();
        }
    }

    public boolean checkKey(String id) {
        if(projectRepository.findOne(id) != null) {
            return projectRepository.findOne(id).getId().equals(id);
        }
        return false;
    }

    public List<Project> getSortedList(String id, String method) {
        final List<Project> projects = projectRepository.findAll(id);
        sorting(method, projects);
        return projects;
    }
    private void sorting(@NotNull final String method, @NotNull final List<Project> projects) {
        switch (method) {
            case "by creating date":
                projects.sort(Comparator.comparing(Project::getCreatingDate));
                break;
            case "by start date":
                projects.sort(Comparator.comparing(Project::getStartDate));
                break;
            case "by end date":
                projects.sort(Comparator.comparing(Project::getEndDate));
                break;
            case "by status":
                projects.sort(Comparator.comparing(Project::getStatus));
                break;
        }
    }

    public List<Project> find(String id, String part){
        Collection<Project> values = projectRepository.findAll(id);
        List<Project> projects = new ArrayList<>();
        for (Project project : values) {
            if (project.getName().contains(part) || project.getDescription().contains(part)) {
                projects.add(project);
            }
        }
        return projects;
    }

}
