package ru.shumov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;

import lombok.Setter;
import ru.shumov.tm.bootstrap.Bootstrap;
import ru.shumov.tm.entity.Session;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.exceptions.ValidateException;
import ru.shumov.tm.repository.SessionRepository;
import ru.shumov.tm.repository.SessionRepositoryImpl;

import java.util.Collection;
import java.util.UUID;
@NoArgsConstructor
@Getter
@Setter
public class SessionServiceImpl implements SessionService{
    private final String id = UUID.randomUUID().toString();
    private final SessionRepository sessionRepository = new SessionRepositoryImpl();
    private Bootstrap bootstrap;

    public SessionServiceImpl(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void validate(final Session session) throws ValidateException {
        if(session == null) throw new ValidateException();
        if(session.getSignature() == null || session.getSignature().isEmpty()) throw new ValidateException();
        if(session.getUserId() == null || session.getUserId().isEmpty()) throw new ValidateException();
        if(session.getTimestamp() == null) throw new ValidateException();
        final Session value = session.clone();
        final String sessionSignature = session.getSignature();
        final String valueSignature = sing(value).getSignature();
        final boolean check = sessionSignature.equals(valueSignature);
        if(!check) throw new ValidateException();
        if(!sessionRepository.checkKey(session.getId())) throw new ValidateException();
    }

    public Session sing(final Session session) {
        if(session == null) {return null;}
        session.setSignature(null);
        final String signature = SignatureService.sign(session, "Ништяк пацаны, сегодня кайфуем", 365);
        session.setSignature(signature);
        return session;
    }

    public Session openSession(final String login) throws ValidateException {
        final User user = bootstrap.getUserService().getOne(login);
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setOwnerId(id);
        session.setTimestamp(1000L);
        final Session finalSession = sing(session);
        if(finalSession == null) throw new ValidateException();
        sessionRepository.persist(finalSession);
        return finalSession;
    }

    public void closeSession(final Session session) throws ValidateException {
        validate(session);
        sessionRepository.remove(session.getId());
    }

    public Collection<Session> getSessionList() {
        return sessionRepository.findAll();
    }
}
