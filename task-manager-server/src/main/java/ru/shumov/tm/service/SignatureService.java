package ru.shumov.tm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@NoArgsConstructor
public class SignatureService {

    public static String sign(final Object value, String salt, Integer cycle){
        try{
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (final JsonProcessingException e){
            return null;
        }
    }


    private static String sign(String value, String salt, Integer cycle) {
        if(value == null || salt == null || cycle == null) return null;
        String result = value;
        try {
            for (int i = 0; i < cycle; i++) {
                result = md5(salt + value + salt);
            }
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String md5(String string) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(string.getBytes());
        byte[] digest = md.digest();
        return String.format("%032X", new BigInteger(1, digest));
    }
}
