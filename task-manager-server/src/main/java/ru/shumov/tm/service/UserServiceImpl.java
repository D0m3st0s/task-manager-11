package ru.shumov.tm.service;

import jakarta.xml.bind.JAXBContext;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.shumov.tm.dto.UserDTO;
import ru.shumov.tm.entity.User;
import ru.shumov.tm.enums.Role;
import ru.shumov.tm.repository.UserRepository;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.UUID;

import static jakarta.xml.bind.JAXBContext.newInstance;

public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private Md5Service md5Service;

    public UserServiceImpl(UserRepository userRepository, Md5Service md5Service) {
        this.userRepository = userRepository;
        this.md5Service = md5Service;
    }

    public Collection<User> getList() {
        return userRepository.findAll();
    }

    public User getOne(@NotNull String login) {
        return userRepository.findOne(login);
    }

    public void create(@NotNull String login, String userPassword) {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setLogin(login);
        try {
            String password = md5Service.md5(userPassword);
            user.setPassword(password);
        }
        catch (NoSuchAlgorithmException e) {throw new RuntimeException(e);}
        user.setRole(Role.USER);
        userRepository.persist(user);
        save();
    }

    public void update(@NotNull User user) {
        userRepository.merge(user);
    }

    @SneakyThrows
    public void passwordUpdate(User user, String password) {
        var md5Password = md5Service.md5(password);
        user.setPassword(md5Password);
        userRepository.merge(user);
    }

    public void save() {
        UserDTO userDTO = new UserDTO();
        try {
            JAXBContext jaxbContextProjectDTO =  newInstance(UserDTO.class);
            var marshallerUserDTO = jaxbContextProjectDTO.createMarshaller();
            marshallerUserDTO.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            Collection<User> users = userRepository.findAll();
            for(User obj : users) {
                userDTO.getUsers().add(obj);
            }
            marshallerUserDTO.marshal(userDTO, new File("JAXBJsonUsers.json"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
